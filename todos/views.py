from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem
# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    fields = ["name", "created_on"]
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    #watch out for the naming of the model!!! >
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/create.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/edit.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
    # when update was sucessful it goes to above url name
    # with Todolist.id


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todos/itemcreate.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todos/itemedit.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
